/*console.log("Go Sir Hernan!");*/

//fetch()

//This is a method in JavaScript that is used to send request in the server and load the information (response from the server) in the webpages.

//Syntax:
//fetch ("urlAPI", {optional/request from the user and response to the user})

// API: https://jsonplaceholder.typicode.com/posts

/*let specificPost = (id) =>{

	if(id <= 100 && id >= 1){
		fetch(`https://jsonplaceholder.typicode.com/posts/${id}`)
		.then( response => response.json())
		.then( data => console.log(data));

	}
}

specificPost(5);*/
// fetching all the elements in the db

let fetchPosts = () => {
  fetch("https://jsonplaceholder.typicode.com/posts")
    .then((response) => response.json())
    .then((data) => {
      console.log(data);

      return showPosts(data);
    });
};

fetchPosts();

const showPosts = (posts) => {
  let postEntries = "";

  posts.forEach((post) => {
    postEntries += `
		<div id = "post-${post.id}">
			<h3 id = "post-title-${post.id}">${post.title}</h3>
			<p id = "post-body-${post.id}">${post.body}</p>
			<button onclick = "editPost('${post.id}')">Edit</button>
			<button onclick ="deletePost('${post.id}')">Delete</button>
		</div>
		`;
  });

  document.querySelector("#div-post-entries").innerHTML = postEntries;
};

// Add Post

document.querySelector("#form-add-post").addEventListener("submit", (event) => {
  event.preventDefault();

  let title = document.querySelector("#txt-title").value;
  let body = document.querySelector("#txt-body").value;

  fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "POST",
    body: JSON.stringify({
      title: title,
      body: body,
      userId: 1,
    }),
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      alert("Successfully Added.");

      document.querySelector("#txt-title").value = null;
      document.querySelector("#txt-body").value = null;
    });
});

// Edit Post
const editPost = (id) => {
  let title = document.querySelector(`#post-title-${id}`).innerHTML;
  let body = document.querySelector(`#post-body-${id}`).innerHTML;

  document.querySelector("#txt-edit-title").value = title;
  document.querySelector("#txt-edit-body").value = body;
  document.querySelector("#txt-edit-id").value = id;
  // .removeAttribute will remove the attribute from the selected element
  document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

document
  .querySelector("#form-edit-post")
  .addEventListener("submit", (event) => {
    event.preventDefault();

    let id = document.querySelector("#txt-edit-id").value;
    let title = document.querySelector("#txt-edit-title").value;
    let body = document.querySelector("#txt-edit-body").value;

    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
      method: "PATCH",
      body: JSON.stringify({
        title: title,
        body: body,
      }),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        alert("Successfully updated");

        document.querySelector("#txt-edit-id").value = null;
        document.querySelector("#txt-edit-title").value = null;
        document.querySelector("#txt-edit-body").value = null;

        document
          .querySelector("#btn-submit-update")
          .setAttribute("disabled", true);
      });
  });

const deletePost = async (id) => {
  /*console.log("Hi Im from delete Function");*/

  await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
    method: "DELETE",
  })
    .then((response) => response.json())
    .then((data) => {
      alert("Post successfully deleted!");
    });

  document.querySelector(`#post-${id}`).remove();
};
